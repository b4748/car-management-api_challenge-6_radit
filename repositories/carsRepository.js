const { cars } = require("../models");

class CarsRepository {
    static async create({
        plate,
        manufacture,
        model,
        image,
        rent_per_day,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        available_at,
        is_with_driver,
        created_by,
    }) {
        const createdCar = cars.create({
            plate,
            manufacture,
            model,
            image,
            rent_per_day,
            capacity,
            description,
            transmission,
            type,
            year,
            options,
            specs,
            available_at,
            is_with_driver,
            created_by,
        });

        return createdCar;
    }

    static async getAll() {
        const carsData = await cars.findAll();
        return carsData;
    }

    static async updateCars({
        id,
        plate,
        manufacture,
        model,
        image,
        rent_per_day,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        available_at,
        is_with_driver,
        updated_by,
    }) {
        const updatedCar = cars.update({
            plate,
            manufacture,
            model,
            image,
            rent_per_day,
            capacity,
            description,
            transmission,
            type,
            year,
            options,
            specs,
            available_at,
            is_with_driver,
            updated_by,
        },{
            where: {
                id
            }
        });

        return updatedCar;
    }

    static async deletedCars({
        id
    }) {
        const deletedCar = await cars.destroy({
            where: {
                id
            }
        });

        return deletedCar;
    }

    static async getByAvailable({ available_at, is_with_driver }) {
        if (available_at && is_with_driver) {
            const availableCars = await cars.findAll({
                where: {
                    available_at, is_with_driver
                }
            });

            return availableCars;
        }

        return cars;
    }
    
}

module.exports = CarsRepository;
